#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$CURRENT_DIR/.envs"

set -x
# get front end list
targets=$(find ~/Dev -mount -maxdepth 5 -type d -name .git | $SEDCMD "s/${HOME//\//\\/}\/[Dd]ev\/\(.*\)\/\.git/\1/;s/\// /g" | $GREPCMD -v "\.terraform" | $GREPCMD -Ev "^($(tmux list-sessions -F "#{session_name}" | $SEDCMD -n 's/@/ /gp' | xargs -r -L1 zsh -c "V=(\$0 \$@); echo \${(@Oa)V}" | tr '\n' '|' | $SEDCMD 's/|$//'))$" | $SEDCMD 's/^\(.*\)$/dev \1/'; $SEDCMD -n 's/^function \([^_][^(]\+\)().*/\1/gp' $(which tm) | $GREPCMD -Ev "^($(tmux list-sessions -F "#{session_name}" | $GREPCMD -Ev "(@|^dev$)" | tr '\n' '|' | $SEDCMD 's/|$//'))$" | sort | uniq)

resp=$(printf "%s\n[cancel]" "$targets" | eval "$TMUX_FZF_BIN $TMUX_FZF_OPTIONS --print-query")
query=$(printf "%s\n" "$resp" | head -n 1)
target=$(printf "%s\n" "$resp" | tail -n 1)
[[ "$target" == "[cancel]" ]] && exit
if [[ "$target" == "$query" ]]; then
	if [[ $query == new* ]]; then
		query=${query#*new }
		repo=${query%% *}
		dest=${query#* }
		SNAME=$(tm dev $dest || exit 1)
		tmux send -t $SNAME "git clone --recursive $repo ." Enter
	fi
else
	tm $target > /dev/null
fi
