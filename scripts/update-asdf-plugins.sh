#!/bin/sh

SED=$(which gsed 2>&1 >/dev/null && which gsed || echo sed)
GREP=$(which ggrep 2>&1 >/dev/null && which ggrep || echo grep)
_ifs=$IFS; IFS='
'
export IFS

for x in $(asdf latest --all | $GREP missing | $GREP -v stable); do tool=$(echo $x | awk '{ print $1 }'); version=$(echo $x | awk '{ print $2 }'); $SED "s/\\($tool\\) \\([0-9\\.]\\+\\)/$tool $version/" -i ~/.tool-versions; done
export IFS=$_ifs

asdf install
