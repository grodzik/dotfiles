link ("%s/.tmux.conf" % ENV["HOME"]) do
  to ("%s/.config/tmux.conf" % ENV["HOME"])
end

directory ("%s/.tmux/plugins/" % ENV["HOME"]) do 
  recursive true
  action :create
end

git ("%s/.tmux/plugins/tmux-fingers" % ENV["HOME"]) do
  repository "https://github.com/Morantron/tmux-fingers.git"
  revision "master"
  action :sync
end

git ("%s/.tmux/plugins/tmux-urlview" % ENV["HOME"]) do
  repository "https://github.com/tmux-plugins/tmux-urlview.git"
  revision "master"
  action :sync
end

git ("%s/.tmux/plugins/tmux-yank" % ENV["HOME"]) do
  repository "https://github.com/tmux-plugins/tmux-yank"
  revision "master"
  action :sync
end

git ("%s/.tmux/plugins/tmux-sessionist" % ENV["HOME"]) do
  repository "https://github.com/tmux-plugins/tmux-sessionist.git"
  revision "master"
  action :sync
end

git ("%s/.tmux/plugins/tmux-fzf" % ENV["HOME"]) do
  repository "https://github.com/sainnhe/tmux-fzf.git"
  revision "master"
  action :sync
end

link ("%s/.tmux/plugins/tmux-fzf/scripts/spoonlab.sh" % ENV["HOME"]) do
  to ("%s/.config/scripts/tmux-fzf-spoonlab.sh" % ENV["HOME"])
end
