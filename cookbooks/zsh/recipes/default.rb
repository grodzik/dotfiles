link ("%s/.zshrc" % ENV["HOME"]) do
  to ("%s/.config/zsh/zshrc" % ENV["HOME"])
end

link ("%s/.zshenv" % ENV["HOME"]) do
  to ("%s/.config/zsh/zshenv" % ENV["HOME"])
end

directory ("%s/.local/share/zsh/3rd_party/" % ENV["HOME"]) do 
  recursive true
  action :create
end

git ("%s/.local/share/zsh/3rd_party/zsh-syntax-highlighting" % ENV["HOME"]) do
  repository "https://github.com/zsh-users/zsh-syntax-highlighting.git"
  revision "master"
  action :sync
end

git ("%s/.local/share/zsh/3rd_party/zsh-autosuggestions" % ENV["HOME"]) do
  repository "https://github.com/zsh-users/zsh-autosuggestions.git"
  revision "master"
  action :sync
end
