directory ("%s/.local/src/asdf/" % ENV["HOME"]) do 
  recursive true
  action :create
end

git ("%s/.local/src/asdf" % ENV["HOME"]) do
  repository "https://github.com/asdf-vm/asdf.git"
  revision "master"
  action :sync
end

