directory ("%s/.local/bin" % ENV["HOME"]) do 
  recursive true
  action :create
end

[ 
  "autostart.sh",
  "tm",
  "iwatch",
  "update-asdf-plugins.sh"
].each do |script|
  link "#{ENV["HOME"]}/.local/bin/#{script}" do 
    to "#{ENV["HOME"]}/.config/scripts/#{script}"
  end
end
