directory ("%s/.config/ranger/plugins" % ENV["HOME"]) do 
  recursive true
  action :create
end

directory ("%s/.local/src/ranger/" % ENV["HOME"]) do 
  recursive true
  action :create
end

git ("%s/.local/src/ranger/devicons" % ENV["HOME"]) do
  repository "https://github.com/alexanderjeurissen/ranger_devicons.git"
  revision "main"
  action :sync
end

link ("%s/.config/ranger/plugins/ranger_devicons" % ENV["HOME"]) do
  to ("%s/.local/src/ranger/devicons" % ENV["HOME"])
end
