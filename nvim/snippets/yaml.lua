function timestamp(args, parent, user_args)
	return string.format("\"%s\"", os.time())
end

function quote(args, parent, user_args)
	return string.format("\"%s\"", args[1][1])
end

snippets = {
	s("automation",
		{
			t({"automation:", "\tid: "}), f(timestamp, {}, {}), 
			t({"", "\talias: \""}), i(1), t("\""),
		}
	),
    s("trigger", {
        i(1, "First jump"),
        t(" :: "),
        sn(2, {
            i(1, "Second jump"),
            t" : ",
            i(2, "Third jump")
        }),
        i(3, "Fourth jump"),
        f(quote, {2}, {})
    })
}

return snippets
