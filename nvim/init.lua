require("plugins")

vim.o.number = true
vim.o.relativenumber = true
vim.o.shiftwidth = 4
vim.o.smarttab = true

vim.g.mapleader = ","

require("mappings")

require("telescope").setup{
  defaults = {
    vimgrep_arguments = {
      'rg',
      '--color=never',
      '--no-heading',
      '--with-filename',
      '--line-number',
      '--column',
      '--smart-case',
      '--hidden',
      '--glob',
      '!{**/.git/*,**/node_modules/*,**/package-lock.json,**/yarn.lock}',
    },
  },
  pickers = {
    find_files = {
      find_command = {
        'rg',
        '--files',
        '--color=never',
        '--line-number',
        '--column',
        '--smart-case',
        '--hidden',
        '--glob',
        '!{**/.git/*,**/node_modules/*}',
      }
    }
  }
}

-- require("cmp-config")

require("null-ls-config")

vim.cmd([[ let g:neo_tree_remove_legacy_commands = 1 ]])

vim.api.nvim_create_user_command("M", "FloatermNew --name=make make <q-args>", {nargs='+'})

vim.g.floaterm_width = 0.4
vim.g.floaterm_height = 0.4
vim.g.floaterm_position = 'rightbelow'
vim.g.floaterm_wintype = 'vsplit'

-- vim.g.python3_host_prog = "/Users"
vim.o.scrolloff = 10

vim.g.tokyodark_color_gamma = "0.8"
vim.cmd[[colorscheme tokyodark]]

vim.cmd [[
  augroup python_black
    autocmd!
    autocmd BufWritePre *.py :call BlackSync()
  augroup end
]]

vim.cmd [[
  augroup commentary
    autocmd!
    autocmd FileType helm setlocal commentstring=#\ %s
    autocmd FileType yaml setlocal commentstring=#\ %s
  augroup end
]]

require('tabnine').setup({
  disable_auto_comment=true,
  accept_keymap="<Tab>",
  dismiss_keymap = "<C-]>",
  debounce_ms = 800,
  suggestion_color = {gui = "#808080", cterm = 244},
  exclude_filetypes = {"TelescopePrompt", "NvimTree"},
  log_file_path = nil, -- absolute path to Tabnine log file
})
