local map = require("utils").map

local opts = { noremap=true, silent=true }
vim.keymap.set('n', ',v', require("jenkinsfile_linter").validate, opts)
