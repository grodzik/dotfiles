local map = require("utils").map

map("n", "<Leader>p", ":1close<CR>:silent Git push -u origin HEAD<CR>")
map("n", "<Leader>P", ":1close<CR>:silent Git push -f<CR>")
