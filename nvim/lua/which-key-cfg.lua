local wk = require("which-key")

vim.o.timeout = true
vim.o.timeoutlen = 300
wk.setup {
  -- your configuration comes here
  -- or leave it empty to use the default settings
  -- refer to the configuration section below
}

wk.add({
    { "<Space>c", group = "ChatGPT" },
    { "<Space>cc", "<cmd>ChatGPT<CR>", desc = "ChatGPT" },
    {
      mode = { "n", "v" },
      { "<Space>ca", "<cmd>ChatGPTRun add_tests<CR>", desc = "Add Tests" },
      { "<Space>cd", "<cmd>ChatGPTRun docstring<CR>", desc = "Docstring" },
      { "<Space>ce", "<cmd>ChatGPTEditWithInstruction<CR>", desc = "Edit with instruction" },
      { "<Space>cf", "<cmd>ChatGPTRun fix_bugs<CR>", desc = "Fix Bugs" },
      { "<Space>cg", "<cmd>ChatGPTRun grammar_correction<CR>", desc = "Grammar Correction" },
      { "<Space>ck", "<cmd>ChatGPTRun keywords<CR>", desc = "Keywords" },
      { "<Space>cl", "<cmd>ChatGPTRun code_readability_analysis<CR>", desc = "Code Readability Analysis" },
      { "<Space>co", "<cmd>ChatGPTRun optimize_code<CR>", desc = "Optimize Code" },
      { "<Space>cr", "<cmd>ChatGPTRun roxygen_edit<CR>", desc = "Roxygen Edit" },
      { "<Space>cs", "<cmd>ChatGPTRun summarize<CR>", desc = "Summarize" },
      { "<Space>ct", "<cmd>ChatGPTRun translate<CR>", desc = "Translate" },
      { "<Space>cx", "<cmd>ChatGPTRun explain_code<CR>", desc = "Explain Code" },
    }
  })
