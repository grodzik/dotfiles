null_ls = require("null-ls")
null_ls_builtins = null_ls.builtins

null_ls.setup({
    sources = {
        null_ls_builtins.formatting.terraform_fmt,
    },
    on_attach = function(client, bufnr)
      if client.supports_method("textDocument/formatting") then
        vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
        vim.api.nvim_create_autocmd("BufWritePre", {
            group = augroup,
            buffer = bufnr,
            -- on 0.8, you should use vim.lsp.buf.format instead
            callback = function()
              vim.lsp.buf.format()
            end
          })
      end
    end,
    debug = true
})
