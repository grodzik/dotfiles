local map = require("utils").map

map("n", '<Leader>qq', ':q!<CR>', { noremap = true, silent = true})
map("n", '<Leader>qa', ':qa!<CR>', { noremap = true, silent = true})
map("n", '<Leader>qw', ':update<CR>', { noremap = true, silent = false})

map('', '<C-p>', ":lua require'telescope.builtin'.find_files{hidden=true}<CR>", { noremap = true, silent = false})
map('', '<Leader><C-g>', ":lua require'telescope.builtin'.live_grep()<CR>", { noremap = true, silent = false})
map('', '<Leader><C-h>', ":lua require'telescope.builtin'.help_tags()<CR>", { noremap = true, silent = false})
map('', '<Leader><C-s>', ":lua require'telescope.builtin'.symbols()<CR>", { noremap = true, silent = false})

map('', '<Leader>dt', ":windo diffthis<CR>", { noremap = true, silent = false})
map('', '<Leader>do', ":windo diffoff<CR>", { noremap = true, silent = false})

map("n", '<Leader>gs', ':Git<CR>', { noremap = true, silent = false})


map("n", 'M', ':M ', { noremap = true, silent = false})
map("n", "<C-f>", ":Neotree float filesystem<CR>", { noremap = true, silent = false })
map("n", "<C-t>", ":FloatermToggle scratchpad<CR>", { noremap = true, silent = true })
map("t", "<C-t>", "<C-\\><C-n>:FloatermToggle scratchpad<CR>", { noremap = true, silent = true })
map("t", "<S-PageUp>", "<C-\\><C-n><S-PageUp>", { noremap = true, silent = true })
map("n", "<C-c>", ":ChatGPT<CR>", { noremap = true, silent = false })

map("n", "[t", ":tabprevious<CR>", { noremap = true, silent = false })
map("n", "]t", ":tabnext<CR>", { noremap = true, silent = false })

map("n", "<Leader>glo", ":silent! Git log --pretty='%Cred%h %Cgreen(%cr) %C(bold blue)<%an>%Creset -%C(yellow)%d%Creset %s %Creset' --abbrev-commit<CR>")

local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

map("t", "<Esc>", "<C-\\><C-n>", { noremap = true, silent = true })
map("t", "<C-e>", "<Esc>", { noremap = false, silent = true })
map("t", "<C-v><Esc>", "<Esc>", { noremap = true, silent = true })
map("t", "<C-h>", "<Esc><C-w>h", { noremap = false, silent = true })
map("t", "<C-j>", "<Esc><C-w>j", { noremap = false, silent = true })
map("t", "<C-k>", "<Esc><C-w>k", { noremap = false, silent = true })
map("t", "<C-l>", "<Esc><C-w>l", { noremap = false, silent = true })
