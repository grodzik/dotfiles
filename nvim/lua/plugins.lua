local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

return require('packer').startup(function()
    use 'wbthomason/packer.nvim'

    use 'tpope/vim-fugitive'
    use 'tpope/vim-surround'
    use 'tpope/vim-eunuch'
    use 'tpope/vim-commentary'

    use 'voldikss/vim-floaterm'

    use 'neomake/neomake'

    use 'christoomey/vim-tmux-navigator'

    use 'sheerun/vim-polyglot'

    use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
    }
    use 'nvim-telescope/telescope-symbols.nvim'
    use {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v3.x",
    requires = { 
        "nvim-lua/plenary.nvim",
        "kyazdani42/nvim-web-devicons", -- not strictly required, but recommended
        "MunifTanjim/nui.nvim",
    }
    }

    use({
        "jose-elias-alvarez/null-ls.nvim",
        requires = { "nvim-lua/plenary.nvim" },
    })

    use {
        "nathom/filetype.nvim",
        config = function()
            require("filetype").setup {
                overrides = {
                    extensions = {
                        tf = "terraform",
                        tfvars = "terraform",
                        tfstate = "json",
                    },
                },
            }
        end,
    }

    use 'gabesoft/vim-ags'

    use 'Yggdroot/indentLine'

    use "neovim/nvim-lspconfig"

    use {
        "hrsh7th/nvim-cmp",
        requires = {
            {"hrsh7th/cmp-nvim-lsp"},
            {"hrsh7th/cmp-buffer"},
            {"hrsh7th/cmp-path"},
            {"hrsh7th/cmp-cmdline"},
            {"hrsh7th/cmp-nvim-lua"},
            {"L3MON4D3/LuaSnip"},
            {"saadparwaiz1/cmp_luasnip"},
            {'tzachar/cmp-tabnine', run='./install.sh'},
            {"rafamadriz/friendly-snippets"},
        }
    }
    use { 'codota/tabnine-nvim', run = "./dl_binaries.sh" }

    use 'tiagovla/tokyodark.nvim'
    use 'averms/black-nvim'
    use({
      "jackMort/ChatGPT.nvim",
        config = function()
          require("chat-gpt-cfg")
        end,
        requires = {
          "MunifTanjim/nui.nvim",
          "nvim-lua/plenary.nvim",
          "nvim-telescope/telescope.nvim"
        }
    })
    use({
        'topaxi/gh-actions.nvim',
        config = function(_, opts)
          require('gh-actions').setup(opts)
        end,
        requires = {
          'nvim-lua/plenary.nvim',
          'MunifTanjim/nui.nvim'
        }
    })
    use {
      "folke/which-key.nvim",
      config = function()
        require("which-key-cfg")
      end
    }
    if packer_bootstrap then
      require('packer').sync()
    end

    use({'ckipp01/nvim-jenkinsfile-linter', requires = { "nvim-lua/plenary.nvim" } })
end)
