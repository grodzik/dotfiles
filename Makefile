COOKBOOKS := $(notdir $(wildcard cookbooks/*))

.PHONY: all $(COOKBOOKS)

all: $(COOKBOOKS)

new:
	mkdir -p cookbooks/${CB}/recipes/
	touch cookbooks/${CB}/recipes/default.rb
	sed "s/NAME/${CB}/g" targets/template.json > targets/${CB}.json

$(COOKBOOKS):
	cinc-solo -c solo.rb -j targets/$@.json
